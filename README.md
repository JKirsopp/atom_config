Keep up to date as new packages are installed and/or configuration of atom is changed.
How to use:

    1. $ snap install atom
    2. $ connect .atom directory with repo
    3. remove config.cson 
    4. clone directory
    5. mv bits to .atom
    6. install bits https://stackoverflow.com/questions/30006827/how-to-save-atom-editor-config-and-list-of-packages-installed